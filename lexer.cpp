#include "lexer.hpp"

Token::Token(TokenType type, std::string value)
: Type(type), Value(value) { }

Token::Token(TokenType type) : Type(type) { }

Lexer::Lexer(std::istream & inStream) : inStream(inStream) {
	token = NULL;
	NextToken();
}

Lexer::~Lexer() {
	if (token)
		delete token;
}

void Lexer::ChangeToken(Token * newToken) {
	if (token)
		delete token;
	token = newToken;
}

void Lexer::NextToken() {
	char c = NextChar();

	/* loads variable */
	if (isalpha(c)) {
		std::string tmp(1, c);
		while (isalpha(c = NextChar()))
			tmp += c;
		inStream.putback(c);
		ChangeToken(new Token(Var, tmp));
		return;
	}

	switch (c) {
		case '(': ChangeToken(new Token(LParen));
		return;
		case ')': ChangeToken(new Token(RParen));
		return;
		case '.': ChangeToken(new Token(Dot));
		return;
		case -50:
			if (NextChar() == -69) // lambda
				ChangeToken(new Token(Lambda));
			else
				throw std::string("Invalid character: '" + std::string(1, c)) + "'";
		return;
		case '\0': ChangeToken(new Token(End));
		return;
		/* skips white spaces */
		case ' ':
		case '\t':
		case '\n':
			NextToken();
		return;
		default:
			throw std::string("Invalid character: '" + std::string(1, c)) + "'";
	}
}

char Lexer::NextChar() const {
	char c;
	if (inStream.get(c))
		return c;
	else
		return '\0';
}

bool Lexer::IsEqual(TokenType type) {
	return token->Type == type;
}

bool Lexer::Skip(TokenType type) {
	if (IsEqual(type)) {
		NextToken();
		return true;
	}
	return false;
}

Token Lexer::Get(TokenType type) {
	Token prev = *token;
	Match(type);
	return prev;
}

std::string ToString(TokenType type) {
	switch (type) {
		case LParen: return "(";
		case RParen: return ")";
		case Lambda: return "λ";
		case Dot: return ".";
		case Var: return "[Variable name]";
		case End: return "EOF";
	}
	throw std::string("Untouchable code hit (at lexer.cpp)");
}

bool Lexer::Match(TokenType type) {
	if (IsEqual(type)) {
		NextToken();
		return true;
	}
	throw std::string("Expected: '" + ToString(type) + "', got: " + ToString(token->Type));	
}