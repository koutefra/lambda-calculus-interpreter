#ifndef LEXER_H
#define LEXER_H

#include <iostream>
#include <fstream>
#include <string>

enum TokenType {
	LParen,
	RParen,
	Lambda,
	Dot,
	Var,
	End
};

std::string ToString(TokenType type);

class Token {
public:
	Token(TokenType type, std::string value);
	Token(TokenType type);
	const TokenType Type;
	const std::string Value;
};

class Lexer {
public:
	Lexer(std::istream & inStream);
	~Lexer();
	bool Skip(TokenType type);
	bool IsEqual(TokenType type);
	bool Match(TokenType type);
	Token Get(TokenType type);
private:
	void ChangeToken(Token * newToken);
	void NextToken();
	char NextChar() const;
	Token * token;
	std::istream & inStream;
};

#endif /* LEXER_H */