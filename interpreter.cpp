#include "interpreter.hpp"

Interpreter::Interpreter(std::istream & inStream, std::ostream & outStream)
: outStream(outStream) {
	syntaxTree = NULL;

	Parser parser(inStream);
	syntaxTree = parser.Parse();
}

Interpreter::~Interpreter() {
	if (syntaxTree)
		delete syntaxTree;
}

void Interpreter::PrintVarStatus() const {
	outStream << syntaxTree->GetVarStatus({ });
}

void Interpreter::Evaluate() const {
	int steps = 0;
	do {
		if (steps == 0)
			outStream << "original: ";
		else
			outStream << "  step " << steps << ": ";

		outStream << syntaxTree->ToString() << std::endl;
		steps++;
	} while (syntaxTree->AlphaReduce());
}