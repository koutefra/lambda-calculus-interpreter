#ifndef SYNTAXTREE_H
#define SYNTAXTREE_H

#include <list>
#include <string>

enum ExprType {
	Id,
	Func,
	App
};

class Expression { 
public:
	Expression(ExprType type);
	virtual ~Expression() = default;
	virtual std::string ToString() const = 0;
	virtual std::string GetVarStatus(std::list<std::string> boundVars) const = 0;
	virtual bool AlphaReduce() = 0;
	virtual Expression * Substitude(std::string varName, Expression * applicant) = 0;
	virtual Expression * Copy() const = 0;
	const ExprType Type;
	static bool WasLastPrintId;
}; 

class Identifier: public Expression {
public:
	Identifier(std::string varName);
	std::string ToString() const override;
	std::string GetVarStatus(std::list<std::string> boundVars) const override;
	bool AlphaReduce() override;
	Expression * Substitude(std::string varName, Expression * applicant) override;
	Expression * Copy() const override;
	std::string VarName;
};

class Function: public Expression {
public:
	Function(std::string varName, Expression * body);
	~Function();
	std::string ToString() const override;
	std::string GetVarStatus(std::list<std::string> boundVars) const override;
	bool AlphaReduce() override;
	Expression * Substitude(std::string varName, Expression * applicant) override;
	Expression * Copy() const override;
	std::string VarName;
	Expression * Body;
};

class Application: public Expression {
public:
	Application(std::list<Expression *> * exprs);
	~Application();
	std::string ToString() const override;
	std::string GetVarStatus(std::list<std::string> boundVars) const override;
	bool AlphaReduce() override;
	Expression * Substitude(std::string varName, Expression * applicant) override;
	Expression * Copy() const override;
	std::list<Expression *> * Exprs;
};

#endif /* SYNTAXTREE_H */