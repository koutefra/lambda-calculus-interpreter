#include "syntaxTree.hpp"

bool Expression::WasLastPrintId = false;

Expression::Expression(ExprType type) : Type(type) { }

Identifier::Identifier(std::string varName) : Expression(Id) {
	VarName = varName;
}

std::string Identifier::ToString() const {
	if (WasLastPrintId)
		return " " + VarName;

	WasLastPrintId = true;
	return VarName;
}

std::string Identifier::GetVarStatus(std::list<std::string> boundVars) const {
	for (auto it: boundVars) {
		if (it == VarName)
			return VarName + " bound\n";
	}
	return VarName + " free\n";
}

bool Identifier::AlphaReduce() {
	return false;
}

Expression * Identifier::Substitude(std::string varName, Expression * applicant) {
	if (VarName == varName)
		return applicant->Copy();
	return this;
}

Expression * Identifier::Copy() const {
	return new Identifier(VarName);
}

Function::Function(std::string varName, Expression * body) : Expression(Func) {
	VarName = varName;
	Body = body;
}

std::string Function::ToString() const {
	WasLastPrintId = false;
	return "(λ" + VarName + "." + Body->ToString() + ")";
}

Function::~Function() {
	delete Body;
}

std::string Function::GetVarStatus(std::list<std::string> boundVars) const {
	boundVars.push_back(VarName);
	return Body->GetVarStatus(boundVars);
}

bool Function::AlphaReduce() {
	return Body->AlphaReduce();
}

Expression * Function::Substitude(std::string varName, Expression * applicant) {
	Body->Substitude(varName, applicant);
	return this;
}

Expression * Function::Copy() const {
	return new Function(VarName, Body->Copy());
}

Application::Application(std::list<Expression *> * exprs) : Expression(App) {
	Exprs = exprs;
}

std::string Application::ToString() const {
	std::string res = "";
	for (auto it: *Exprs) {
		if (it->Type == App) {
			WasLastPrintId = false;
			res += ("(" + it->ToString() + ")");
		}
		else
			res += it->ToString();
	}
	WasLastPrintId = false;
	return res;
}

Application::~Application() {
	for (auto it: *Exprs)
		delete it;
	delete Exprs;
}

std::string Application::GetVarStatus(std::list<std::string> boundVars) const {
	std::string res = "";
	for (auto it: *Exprs)
		res += it->GetVarStatus(boundVars);
	return res;
}

bool Application::AlphaReduce() {
	for (auto it = Exprs->begin(); it != Exprs->end(); it++) {
		auto applicant = std::next(it, 1);
		if ((*it)->Type == Func && applicant != Exprs->end()) {
			Function * castedFunc = static_cast<Function*>(*it);
			Expression * newComponent = castedFunc->Body;
			*it = newComponent->Substitude(castedFunc->VarName, *applicant);
			Exprs->erase(applicant);
			return true;
		}
	}

	for (auto it = Exprs->begin(); it != Exprs->end(); it++) {
		if ((*it)->AlphaReduce()) {
			if ((*it)->Type == App) {
				Application * castedApp = static_cast<Application*>(*it);
				if (castedApp->Exprs->size() == 1) {
					Exprs->push_back(castedApp->Exprs->front());
					delete castedApp->Exprs;
					Exprs->erase(it);
				}
			}
			return true;
		}
	}
	return false;
}

Expression * Application::Substitude(std::string varName, Expression * applicant) {
	for (auto it = Exprs->begin(); it != Exprs->end(); it++)
		*it = (*it)->Substitude(varName, applicant);
	return this;
}

Expression * Application::Copy() const {
	std::list<Expression *> * exprs = new std::list<Expression *>();

	for (auto it: *Exprs)
		exprs->push_back(it->Copy());
	return new Application(exprs);
}