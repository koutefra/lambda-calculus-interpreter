#include "parser.hpp"

Parser::Parser(std::istream & inStream) {
	lexer = new Lexer(inStream);
}

Parser::~Parser() {
	delete lexer;
}

Expression * Parser::Parse() {
	Expression * expr = ParseApp();
	
	lexer->Match(End);
	return expr;
}

Expression * Parser::ParseTerm() {
	if (lexer->Skip(Lambda)) {
		std::string id = lexer->Get(Var).Value;
		lexer->Match(Dot);
		return new Function(id, ParseTerm());
	}
	return ParseApp();
}

Expression * Parser::ParseApp() {
	std::list<Expression *> * exprs = new std::list<Expression *>();

	Expression * next = ParseAtom();
	while (next) {
		exprs->push_back(next);
		next = ParseAtom();
	}

	if (exprs->size() == 0)
		throw std::string("Invalid application");
	return new Application(exprs);
}

Expression * Parser::ParseAtom() {
	if (lexer->Skip(LParen)) {
		Expression * term = ParseTerm();
		lexer->Match(RParen);
		return term;
	} else if (lexer->IsEqual(Var)) {
		std::string id = lexer->Get(Var).Value;
		return new Identifier(id);
	}
	return NULL;
}