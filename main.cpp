#include "interpreter.hpp"
#include <string.h>

void usage() {
	std::cout << "λ-calculus interpreter" << std::endl
		<< "Evaluate λ-calculus source code with all middle-steps." << std::endl
		<< "	-v	Analyse status of all variables" << std::endl
		<< "	-h	Display this help message." << std::endl
		<< "2018 Frantisek Koutensky" << std::endl;
}

int main(int argc,char* argv[]) {
	try {
		if (argc == 1) {
			Interpreter interpreter(std::cin, std::cout);
			interpreter.Evaluate();
		} else if (argc == 2 && strcmp(argv[1], "-v") == 0) {
			Interpreter interpreter(std::cin, std::cout);
			interpreter.PrintVarStatus();
		}
		else if (argc == 2 && strcmp(argv[1], "-h") == 0)
			usage();
		else
			std::cout << "Invalid args. Use '-h' for help." << std::endl;
	} catch (std::string e) {
		std::cout << "Exception: " << e << std::endl;
	}
	return 0;
}