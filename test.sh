if [ $# -ne 1 ] ; then
	echo "Pass tested program as argument"
	exit 1
fi

VARTESTS_CNT=3
VARTESTS_PATH=./Tests/VarStatusTests/

echo "Testing mode '-v'"
for ((i=0; i<$VARTESTS_CNT;i++));
do
	"$1" -v < "$VARTESTS_PATH"in$i.txt > "$VARTESTS_PATH"out$i.txt

	diff "$VARTESTS_PATH"out$i.txt "$VARTESTS_PATH"out_ref$i.txt > /dev/null

	if [ $? -eq 0 ] ; 
	then
		echo "Test $i OK"
	else
		echo "Test $i failed"
	fi
done

exit 0