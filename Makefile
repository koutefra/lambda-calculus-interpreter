CXX=g++
CXXFLAGS=-std=c++14 -Wall -pedantic -Wno-long-long -O2
RM=rm -f

SRCS=main.cpp lexer.cpp syntaxTree.cpp parser.cpp interpreter.cpp
OBJS=$(subst .cpp,.o,$(SRCS))
TARGET=lambdaEval

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CXX) $(CXXFLAGS) -o $(TARGET) $(OBJS)

main.o: main.cpp interpreter.hpp parser.hpp lexer.hpp syntaxTree.hpp
lexer.o: lexer.cpp lexer.hpp
syntaxTree.o: syntaxTree.cpp syntaxTree.hpp
parser.o: parser.cpp parser.hpp lexer.hpp syntaxTree.hpp
interpreter.o: interpreter.cpp interpreter.hpp parser.hpp lexer.hpp \
 syntaxTree.hpp
lexer.o: lexer.hpp
syntaxTree.o: syntaxTree.hpp
interpreter.o: interpreter.hpp parser.hpp lexer.hpp syntaxTree.hpp
parser.o: parser.hpp lexer.hpp syntaxTree.hpp

clean:
	$(RM) $(OBJS)
