#ifndef INTERPRETER_H
#define INTERPRETER_H

#include "parser.hpp"
#include <iostream>

class Interpreter {
public:
	Interpreter(std::istream & inStream, std::ostream & outStream);
	~Interpreter();
	void PrintVarStatus() const;
	void Evaluate() const;
private:
	Expression * syntaxTree;
	std::ostream & outStream;
};

#endif /* INTERPRETER_H */