#ifndef PARSER_H
#define PARSER_H

#include "lexer.hpp"
#include "syntaxTree.hpp"

class Parser {
public:
	Parser(std::istream & inStream);
	~Parser();
	Expression * Parse();
private:
	Expression * ParseTerm();
	Expression * ParseApp();
	Expression * ParseAtom();
	Lexer * lexer;
};

#endif /* PARSER_H */